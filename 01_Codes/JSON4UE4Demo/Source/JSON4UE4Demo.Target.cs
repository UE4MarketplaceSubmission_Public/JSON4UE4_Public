/*
*  Copyright (c) 2016-2018 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2018/05/03
*
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class JSON4UE4DemoTarget : TargetRules
{
	public JSON4UE4DemoTarget(TargetInfo Target) : base(Target)
    {
		Type = TargetType.Game;

        ExtraModuleNames.AddRange(new string[] { "JSON4UE4Demo" });
    }
}
